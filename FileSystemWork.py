import cv2
import os
import glob
import numpy

path = os.getcwd() + '/resource/pictures/sample_work'
color = ['red', 'green', 'blue']

print('\n <--program starts here-->')

fileList = glob.glob(os.path.join(path, '*.jpg'))

for fileName in fileList:
    #print(fileName)
    rgb = cv2.imread(fileName)
    rgb = cv2.resize(rgb, (700, 350))
    cv2.imshow("rgb", rgb)

while 1:

    if cv2.waitKey(1) == ord("q"):
        break

cv2.destroyAllWindows()