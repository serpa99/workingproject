import cv2
import os
import glob
import numpy

path = os.getcwd() + '/resource/pictures/sample_work'
color = ['red', 'green', 'blue']
i = 0

print('\n<--Enter n to pick next sample, q to exit-->')
print('<--program starts here-->')

fileList = glob.glob(os.path.join(path, '*.jpg'))

while len(fileList) > i:

    # выбрать следующую картинку
    if cv2.waitKey(1) == ord("n") or i == 0:
        # начало процедуры
        cv2.destroyAllWindows()

        # описание исходного файла
        fileName = fileList[i]
        frame = cv2.imread(fileName)
        frame = cv2.resize(frame, (60, 120))
        cv2.imshow(str(i), frame)

        # интервал строк и столбцов для анализа
        cutedFrame = frame[20:101, 15:48]
        cv2.imshow("cutedFrame" + str(i), cutedFrame)

        hsv = cv2.cvtColor(cutedFrame, cv2.COLOR_BGR2HSV)
        v = hsv[:, :, 2]
        cv2.imshow("v " + str(i), v)

        red_sum = numpy.sum(v[0:27, 0:44])
        yellow_sum = numpy.sum(v[28:54, 0:44])
        green_sum = numpy.sum(v[55:81, 0:44])

        cv2.rectangle(cutedFrame, (0, 0), (44, 27), (0, 0, 255), 2)
        cv2.rectangle(cutedFrame, (0, 28), (44, 54), (0, 255, 255), 3)
        cv2.rectangle(cutedFrame, (0, 55), (44, 81), (0, 255, 0), 3)
        cv2.imshow("frameCopy" + str(i), cutedFrame)

        print(str(red_sum) + " : " + str(yellow_sum) + " : " + str(green_sum))

        # обработка областей по цвету
        if green_sum > yellow_sum and green_sum > red_sum:
            print("green")
        elif yellow_sum > green_sum and yellow_sum > red_sum:
            print("yellow")
        elif red_sum > green_sum and red_sum > yellow_sum:
            print("red")
        else:
            print("idk")

        # конец процедуры
        i = i + 1
        #print("this is i<" + str(i) + ">")

    # завершить работу
    if cv2.waitKey(1) == ord("q"):
        break

print("\nThat`s all!!! Press e to continue")

while 1:
    if cv2.waitKey(1) == ord("e"):
        cv2.destroyAllWindows()
        break
