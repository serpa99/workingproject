import cv2
import time
import numpy as np

def def_web(cam, width, height):
    webcam = cv2.VideoCapture(cam)
    webcam.set(3, width)
    webcam.set(4, height)
    return webcam

if __name__ ==  '__main__':
    # название устройства
    webcam = def_web(0, 1280, 720)

    listOfDur = []
    maxLen = 100

    while True:
        timeStart = time.time()
        ret, frame = webcam.read()

        if not ret:
            break

        cv2.imshow('frame name', frame)

        workTime = time.time() - timeStart
        if len(listOfDur) > maxLen:
            listOfDur.remove(listOfDur[0])
        listOfDur.append(workTime)

        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

    print(f'Average time per frame: {round(np.mean(listOfDur)*1000,2)} ms')
    webcam.release()
    cv2.destroyAllWindows()