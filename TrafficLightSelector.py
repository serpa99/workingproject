import cv2
import os
import glob
import numpy

i = 1
while (i<6):
    frame = cv2.imread(str(i) + ".jpg")
    frame = cv2.resize(frame, (60, 120))
    cv2.imshow(str(i), frame)

    cutedFrame = frame[20:101, 8:52]
    cv2.imshow("cutedFrame"+str(i), cutedFrame)

    hsv = cv2.cvtColor(cutedFrame, cv2.COLOR_BGR2HSV)
    v = hsv[:, :, 2]
    cv2.imshow("v " + str(i), v)

    red_sum = numpy.sum(v[0:27, 0:44])
    yellow_sum = numpy.sum(v[0:28, 0:44])
    green_sum = numpy.sum(v[55:81, 0:44])

    cv2.rectangle(cutedFrame, (0, 0), (44, 27), (0, 0, 255), 2)
    cv2.rectangle(cutedFrame, (0, 28), (44, 54), (0, 255, 255), 3)
    cv2.rectangle(cutedFrame, (0, 55), (44, 81), (0, 255, 0), 3)
    cv2.imshow("frameCopy" + str(i), cutedFrame)

    print(str(red_sum) + " : " + str(yellow_sum) + " : " + str(green_sum))

    #обработка областей по цвету
    if green_sum > yellow_sum and green_sum > red_sum:
        print("green")
    elif yellow_sum > green_sum and yellow_sum > red_sum:
        print("yellow")
    elif red_sum > green_sum and red_sum > yellow_sum:
        print("red")
    else:
        print("red")

    key = cv2.waitKey(1)
    if key == ord("n"):
        i = i+1
        #cv2.destroyAllWindows()

    if key == ord("q"):
        break

cv2.destroyAllWindows()